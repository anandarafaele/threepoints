import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final String userTable = "userTable";
final String idColumn = "idColumn";
final String nameColumn = "nameColumn";
final String emailColumn = "emailColumn";
final String cpfColumn = "cpfColumn";
final String passwordColumn = "passwordColumn";
final String phoneColumn = "phoneColumn";

class UserHelper {

  static final UserHelper _instance = UserHelper.internal();

  factory UserHelper() => _instance;

  UserHelper.internal();

  Database _db;

  Future<Database> get db async {
    if(_db != null) {
      return _db;
    } else {
      _db = await initDb();
      return _db;
    }
  }

  Future<Database> initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "user.db");

    return await openDatabase(path, version: 1, onCreate: (Database db, int newerVersion ) async {
      await db.execute(
        "CREATE TABLE $userTable("
            "$idColumn INTEGER PRIMARY KEY,"
            "$nameColumn TEXT,"
            "$emailColumn TEXT,"
            "$cpfColumn INTEGER,"
            "$passwordColumn TEXT,"
            "$phoneColumn TEXT)"
      );
    });
  }
}

class User {

  int id;
  String name;
  String email;
  int cpf;
  String password;
  String phone;

  User.fromMap(Map map){
    id = map[idColumn];
    name = map[nameColumn];
    email = map[emailColumn];
    cpf = map[cpfColumn];
    password = map[passwordColumn];
    phone = map[phoneColumn];
  }

  Map toMap() {
    Map<String, dynamic> map = {
      nameColumn: name,
      emailColumn: email,
      cpfColumn: cpf,
      passwordColumn: password,
      phoneColumn: phone,
    };
    if(idColumn == null) {
      map[idColumn] = id;
    }
  }

  @override
  String toString() {
    return "User(id: $id, name: $name, email: $email, phone: $phone, cpf: $cpf)";
  }
}