import 'package:flutter/material.dart';
import 'package:three_points/screens/index_screen.dart';

class LoginScreen extends StatelessWidget {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ThreePoints'),
        backgroundColor: Colors.blue[80],
      ),
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Image(image: AssetImage('images/logoIcon.png'), height: 180, width: 180,),
                Text('DATA 26/01/19 | HORA 13:56', textAlign: TextAlign.center,),
                Padding(
                  padding: EdgeInsets.only(top: 30.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelStyle: TextStyle(color: Colors.blue[80]),
                      labelText: "Insira seu ID",
                      icon: Icon(Icons.person),
                      hintText: "UsuarioID",
                    ),
                    validator: (text) {
                      if(text.isEmpty || text.length < 11) return "Usuário inválido";
                    },
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontSize: 18.0),
                  ),
                ),
                TextFormField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelStyle: TextStyle(color: Colors.blue[80]),
                      labelText: "Senha",
                      icon: Icon(Icons.lock),
                      hintText: "********",
                    ),
                    validator: (text) {
                      if(text.isEmpty || text.length < 8) return "Senha inválida";
                    },
                    textAlign: TextAlign.center,
                    obscureText: true,
                    style: TextStyle(color: Colors.black, fontSize: 18.0),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Container(
                    height: 40.0,
                    child: RaisedButton(
                      color: Colors.blue[80],
                      onPressed: () {
                        if(_formKey.currentState.validate()) {
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => IndexScreen()),
                          );
                        }
                      },
                      child: Text("Entrar",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0
                          )
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}