import 'package:flutter/material.dart';

class ReturnScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ThreePoints'),
        backgroundColor: Colors.blue[80],
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Image(
              image: AssetImage('images/logoIcon.png'),
              height: 300,
              width: 300,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  onPressed: () {},
                  child: Image(image: AssetImage('images/location.png'), height: 100, width: 100,),
                ),
                FlatButton(
                  onPressed: () {
                    
                  },
                  child: Image(image: AssetImage('images/photo.png'), height: 100, width: 100,),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 50.0),
              child: Container(
                height: 40.0,
                child: RaisedButton(
                  onPressed: (){},
                  child: Text("CONFIRMAR PONTO", style: TextStyle(color: Colors.white),),
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}
