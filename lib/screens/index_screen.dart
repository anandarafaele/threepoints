import 'package:flutter/material.dart';
import 'package:three_points/screens/checkin.dart';
import 'package:three_points/screens/checkout.dart';
import 'package:three_points/screens/lunch.dart';
import 'package:three_points/screens/return.dart';

class IndexScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ThreePoints'),
        backgroundColor: Colors.blue[80],
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 20.0, bottom: 30.0),
              child: Container(
                width: 140.0,
                height: 140.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage("images/profile.png"),
                      fit: BoxFit.cover
                  )
                ),
              )
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text('Olá, Ananda!', textAlign: TextAlign.center, style: TextStyle(fontSize: 25.0),),
            ),
            Text('DATA 26/01/19 | HORA 13:56', textAlign: TextAlign.center,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 10.0),
                  child: FlatButton(
                    onPressed: (){
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => CheckinScreen()),
                      );
                    },
                    child: Image(image: AssetImage('images/signin.png'), height: 100, width: 100,),
                  )
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 10.0),
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => CheckoutScreen()),
                        );
                      },
                      child: Image(image: AssetImage('images/signout.png'), height: 100, width: 100,),
                    )
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => LunchScreen()),
                        );
                      },
                      child: Image(image: AssetImage('images/lunch.png'), height: 100, width: 100,),
                    )
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => ReturnScreen()),
                        );
                      },
                      child: Image(image: AssetImage('images/back.png'), height: 100, width: 100,),
                    )
                ),
              ],
            ),
          ],
        )
      )
    );
  }

}