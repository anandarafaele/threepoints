import 'package:flutter/material.dart';
//Screen
import 'screens/login_screen.dart';

void main() => runApp(ThreePoints());

class ThreePoints extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.blue[80],
      ),
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}
